namespace Tests

open System.Data
open System.Security.Claims
open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Microsoft.IdentityModel.Protocols.OpenIdConnect
open Microsoft.IdentityModel.Tokens
open Npgsql
open Prometheus.DotNetRuntime
open back_api.Api.Interfaces
open back_api.Authentication.Common
open back_api.Services
open Prometheus


module Program =
    DotNetEnv.Env.Load() |> ignore
    
    let private pgConnectionString = $"""
Server={DotNetEnv.Env.GetString("POSTGRES_HOST_TEST", "127.0.0.1")};
Port={DotNetEnv.Env.GetString "POSTGRES_PORT_TEST"};
Database={DotNetEnv.Env.GetString "POSTGRES_DB_TEST"};
User Id={DotNetEnv.Env.GetString "POSTGRES_USER_TEST"};
Password={DotNetEnv.Env.GetString "POSTGRES_PASSWORD_TEST"};
"""

    [<EntryPoint>]
    let main args =
        let builder = WebApplication.CreateBuilder args

        builder.Services.AddControllers() |> ignore
        builder.Services.AddEndpointsApiExplorer() |> ignore

        builder.Services.AddScoped<IDbConnection>(fun _ -> new NpgsqlConnection(pgConnectionString)) |>ignore
                
        builder.Services.AddSingleton<IRolesService, RolesService>() |> ignore
        
        
        builder.Services.AddAuthentication(
            JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer( fun x ->
                (
                    x.RequireHttpsMetadata <- false
                    x.TokenValidationParameters <- new TokenValidationParameters(
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey()
                    )
                    x.Configuration <- new OpenIdConnectConfiguration()
                )
        ) |> ignore
        builder.Services.AddAuthorization(
            fun opt -> opt.AddPolicy("AdminPolicy", (new AuthorizationPolicyBuilder())
                                                        .RequireAuthenticatedUser()
                                                        .RequireClaim(ClaimTypes.Role, "admin").Build())
            ) |> ignore 
        
        builder.Services.UseHttpClientMetrics() |> ignore
        
        DotNetRuntimeStatsBuilder.Default().StartCollecting() |> ignore
        
        let app = builder.Build()        
                
        0
