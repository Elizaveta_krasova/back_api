module TestsApi

open FSharpBackApi.Controllers
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open Npgsql
open Xunit
open back_api.Api.Entites
open back_api.Api.Interfaces
open NUnit.Framework
open Moq
open back_api.Services
        
            
[<TestFixture>]
type RolesControllerTests () =

    let mockLogger = Mock.Of<ILogger<RolesController>>()
    let mockRolesService = Mock<IRolesService>()
    let createRoleRequests = { role_title = "Admin" }
    let controller = RolesController(mockLogger, mockRolesService.Object)
    let getRoleRequests = {GetRoleResponse.id = 1; GetRoleResponse.role_title = "Admin"}
    let getRoleUpdateRequests = {GetRoleResponse.id = 2; GetRoleResponse.role_title = "User"}
    let updateRoleRequests = {id = 2; role_title = "User"}
    let deleteRoleRequests = {DeleteRoleRequest.id = 1}
    
    // Тест для метода GetAllRoles() в контроллере
    [<Test>]
    member this.``GetAllRoles WithNoRoles ShouldReturnNotFound`` () =
        let loggerMock = new Mock<ILogger<RolesController>>()
        let rolesServiceMock = new Mock<IRolesService>()
        
        rolesServiceMock.Setup(fun x -> x.GetAllRoles())
                        .ReturnsAsync Seq.empty |> ignore
        
        let result = controller.GetAllRoles().Result.Result
        
        match result with
        | :? StatusCodeResult as statusCodeResult ->
            let statusCode = statusCodeResult.StatusCode
            Assert.Equal(404, statusCode)
        | _ ->
            Assert.Fail()
            
    // Тест для метода GetRoleById() в контроллере d
    [<Test>]
    member this.``GetRoleById should return OKRequest``  () =        
        mockRolesService.Setup(fun x -> x.GetRoleById(1))
                        .ReturnsAsync getRoleRequests |> ignore
        
        let result = controller.GetRoleById(1).Result.Result
        
        match result with
        | :? OkObjectResult as statusCodeResult ->
            let statusCode = statusCodeResult.StatusCode
            Assert.Equal(200, statusCode.Value)
        | _ ->
            Assert.Fail()
            
    // Тест для метода GetRoleById() в контроллере
    [<Test>]
    member this.``GetRoleById should return NotFoundRequest``  () =                
        let result = controller.GetRoleById(2).Result.Result
        
        match result with
        | :? StatusCodeResult as statusCodeResult ->
            let statusCode = statusCodeResult.StatusCode
            Assert.Equal(404, statusCode)
        | _ ->
            Assert.Fail()


    // Тест для метода CreateRole() в контроллере
    [<Test>]
    member this.``CreateRole should return OKRequest`` () =
        mockRolesService.Setup(fun x -> x.CreateRole(createRoleRequests))
            .ReturnsAsync(getRoleRequests) |> ignore

        let result = controller.CreateRole(createRoleRequests).Result.Result

        match result with
        | :? OkObjectResult as objectResult ->
            let statusCode = objectResult.StatusCode
            Assert.Equal(200, statusCode.Value)
        | _ ->
            Assert.Fail()

    // Тест для метода UpdateRole() в контроллере
    [<Test>]
    member this.``UpdateRole should return NotFoundRequest`` () =
        let result = controller.UpdateRole(updateRoleRequests).Result.Result

        match result with
        | :? NotFoundResult as statusCodeResult ->
            let statusCode = statusCodeResult.StatusCode
            Assert.Equal(404, statusCode)
        | _ ->
            Assert.Fail()
    
    // Тест для метода UpdateRole() в контроллере
    [<Test>]
    member this.``UpdateRole should return OkRequest`` () =
        mockRolesService.Setup(fun x -> x.GetRoleById(2))
            .ReturnsAsync(getRoleRequests) |> ignore
        mockRolesService.Setup(fun x -> x.UpdateRole(updateRoleRequests))
            .ReturnsAsync(getRoleUpdateRequests) |> ignore

        let result = controller.UpdateRole(updateRoleRequests).Result.Result

        match result with
        | :? OkObjectResult as objectResult ->
            let statusCode = objectResult.StatusCode
            Assert.Equal(200, statusCode.Value)
        | _ ->
            Assert.Fail()


    // Тест для метода DeleteRole() в контроллере
    [<Test>]
    member this.``DeleteRole should return NotFoundRequest`` () =
        controller.CreateRole(createRoleRequests).Result.Result |> ignore
        let result = controller.DeleteRole(deleteRoleRequests).Result.Result

        match result with
        | :? NotFoundResult as statusCodeResult ->
            let statusCode = statusCodeResult.StatusCode
            Assert.Equal(404, statusCode)
        | _ ->
            Assert.Fail()

    // Тест для метода DeleteRole() в контроллере
    [<Test>]
    member this.``DeleteRole should return OkRequest`` () =
        mockRolesService.Setup(fun x -> x.GetRoleById(1))
            .ReturnsAsync(getRoleRequests) |> ignore
        mockRolesService.Setup(fun x -> x.DeleteRole(deleteRoleRequests))
            .ReturnsAsync(getRoleRequests) |> ignore

        let result = controller.DeleteRole(deleteRoleRequests).Result.Result

        match result with
        | :? OkObjectResult as objectResult ->
            let statusCode = objectResult.StatusCode
            Assert.Equal(200, statusCode.Value)
        | _ ->
            Assert.Fail()

[<TestFixture>]
type AuthControllerTests () =

    let mockLogger = Mock.Of<ILogger<UsersController>>()
    let mockUsersService = Mock<IUsersService>()
    let mockRolesService = Mock<IRolesService>()
    let controller = UsersController(mockLogger, mockRolesService.Object, mockUsersService.Object)
    let userLoginRequests = { login = "admin"; password = "string"}
    let userLoginResponse = { id = 1; login = "admin"; role_id = 1 }
    
    
    // Тест для метода UserLogin() в контроллере
    [<Test>]
    member this.``UserLogin Unauthorized`` () =        
        mockUsersService.Setup(fun x -> x.UserLogin(userLoginRequests))
                        .ReturnsAsync userLoginResponse |> ignore
        
        let result = controller.Login(userLoginRequests).Result.Result
        
        match result with
        | :? UnauthorizedResult as statusCodeResult ->
            let statusCode = statusCodeResult.StatusCode
            Assert.Equal(401, statusCode)
        | _ ->
            Assert.Fail()
    

// Валидация почты
[<TestFixture>]
type EmailValidationTests () =
    [<Test>]
    member this.``Valid email should return true`` () =
        let email = "test@example.com"
        Assert.True(Helper.validateEmail email)

    [<Test>]
    member this.``Invalid email should return false`` () =
        let email = "invalid_email"
        Assert.False(Helper.validateEmail email)

    [<Test>]
    member this.``Email without domain should return false`` () =
        let email = "test@"
        Assert.False(Helper.validateEmail email)

    [<Test>]
    member this.``Email without user should return false`` () =
        let email = "@example.com"
        Assert.False(Helper.validateEmail email)

// Валидация номера телефона
[<TestFixture>]
type PhoneNumberValidationTests () =
    [<Test>]
    member this.``Valid phone number should return true`` () =
        let phoneNumber = "+7 (123) 456-78-90"
        Assert.True(Helper.validatePhoneNumber phoneNumber)

    [<Test>]
    member this.``Invalid phone number should return false`` () =
        let phoneNumber = "invalid_number"
        Assert.False(Helper.validatePhoneNumber phoneNumber)

    [<Test>]
    member this.``Phone number without country code should return false`` () =
        let phoneNumber = "(123) 456-78-90"
        Assert.False(Helper.validatePhoneNumber phoneNumber)

    [<Test>]
    member this.``Phone number with incorrect format should return false`` () =
        let phoneNumber = "+1 (123) 456-78-90"
        Assert.False(Helper.validatePhoneNumber phoneNumber)
        
        
DotNetEnv.Env.Load() |> ignore

let private pgConnectionString = $"""
Server=localhost;
Port=5431;
Database=postgres;
User Id=postgres;
Password=postgres;
"""

[<TestFixture>]
type RolesServiceTests() =
    [<Test>]
    member this.``GetAllRoles should return list of roles from database`` () =
        let mockLogger = Mock.Of<ILogger<RolesService>>()
        let dbConnection = new NpgsqlConnection(pgConnectionString)
        let rolesService : IRolesService = RolesService(mockLogger, dbConnection)
        
        let result = rolesService.GetAllRoles().Result

        Assert.Equal(2, result |> Seq.length)
        
    [<Test>]
    member this.``GetRoleById should return list of roles from database`` () =
        let mockLogger = Mock.Of<ILogger<RolesService>>()
        let dbConnection = new NpgsqlConnection(pgConnectionString)
        let rolesService : IRolesService = RolesService(mockLogger, dbConnection)
        
        let result = rolesService.GetRoleById(1).Result

        Assert.Equal(1, result.id)
        Assert.Equal("simple_user", result.role_title)

[<TestFixture>]
type UsersServiceTests() =
    let mockLogger = Mock.Of<ILogger<UsersService>>()
    let dbConnection = new NpgsqlConnection(pgConnectionString)
    let rolesService : IUsersService = UsersService(mockLogger, dbConnection)

    [<Test>]
    member this.``UserLogin should return user lizOk from database`` () =        
        let result = rolesService.UserLogin({login = "lizOk"; password = "1234" }).Result

        Assert.Equal(1, result.id)
        Assert.Equal("lizOk", result.login)
        Assert.Equal(2, result.role_id)
        
    [<Test>]
    member this.``GetUserById should return user lizOk from database`` () =        
        let result = rolesService.GetUserById(1).Result

        Assert.Equal("lizOk", result.login)
        Assert.Equal(2, result.role_id)
        
    [<Test>]
    member this.``UserRegistration should return user from database`` () =        
        let result = rolesService.UserRegistration({
          login = "liza2";
          name = "string";
          middlename = "string";
          lastname = "string";
          email = "string@mail.ru";
          phonenumber = "+7 (913) 224-89-84";
          password = "qwerty";
          role_id = 1}).Result

        Assert.Equal("liza2", result.login)
        Assert.Equal("string", result.name)
        Assert.Equal("string", result.middlename)
        Assert.Equal("string", result.lastname)
        Assert.Equal("string@mail.ru", result.email)
        Assert.Equal("+7 (913) 224-89-84", result.phonenumber)
        Assert.Equal(1, result.role_id)
        
    [<Test>]
    member this.``UserRegistration should return error because email not valid`` () =
        let user ={
          login = "liza2";
          name = "string";
          middlename = "string";
          lastname = "string";
          email = "string";
          phonenumber = "+7 (913) 224-89-84";
          password = "qwerty";
          role_id = 1} 

        Assert.ThrowsAsync(fun () -> rolesService.UserRegistration(user))

    [<Test>]
    member this.``UserLogin should return error`` () =
        let a = 0
        // Assert.Throws(rolesService.UserLogin({ login = "lizOk"; password = "1234jdfghn" }))
        Assert.ThrowsAsync(fun () -> rolesService.UserLogin({ login = "lizOk"; password = "1234jdfghn" })) |> ignore


[<TestFixture>]
type AuthorsServiceTests() =
    let mockLogger = Mock.Of<ILogger<AuthorsService>>()
    let dbConnection = new NpgsqlConnection(pgConnectionString)
    let authorsService : IAuthorsService = AuthorsService(mockLogger, dbConnection)

    [<Test>]
    member this.``GetAllAuthors should return authors from database`` () =        
        let result = authorsService.GetAllAuthors().Result
    
        Assert.Equal(4, result |> Seq.length)
        
    [<Test>]
    member this.``GetAuthorsById should return author from database`` () =
        let result = authorsService.GetAuthorById(1).Result
    
        Assert.Equal("Ноунейм", result.name)
         
    [<Test>]
    member this.``CreateAuthor should create and return author from database`` () =
        let author = { name = "test";}
        let result = authorsService.CreateAuthor(author).Result

        Assert.Equal("test", result.name)
    
[<TestFixture>]
type GenresServiceTests() =
    let mockLogger = Mock.Of<ILogger<GenresService>>()
    let dbConnection = new NpgsqlConnection(pgConnectionString)
    let genresService : IGenresService = GenresService(mockLogger, dbConnection)

    [<Test>]
    member this.``GetAllGenres should return genres from database`` () =        
        let result = genresService.GetAllGenres().Result
    
        Assert.Equal(4, result |> Seq.length)
        
    [<Test>]
    member this.``GetGenreById should return genre from database`` () =
        let result = genresService.GetGenreById(1).Result
    
        Assert.Equal("лирика", result.title)
        Assert.Equal("jnfjsnvdjnfjvn", result.description)
         
    [<Test>]
    member this.``CreateGenre should create and return genre from database`` () =
        let genre = { title = "test"; description = "test2"}
        let result = genresService.CreateGenre(genre).Result

        Assert.Equal("test", result.title)
        Assert.Equal("test2", result.description)
         
    [<Test>]
    member this.``DeleteGenre should delete genre from database`` () =
        let genre = { title = "test"; description = "test2"}
        let result = genresService.CreateGenre(genre).Result
        let delete = genresService.DeleteGenre(result.id).Result

        Assert.Equal(result.title, delete.title)
        Assert.Equal(result.description, delete.description)
    
[<TestFixture>]
type BooksServiceTests() =
    let mockLogger = Mock.Of<ILogger<BooksService>>()
    let dbConnection = new NpgsqlConnection(pgConnectionString)
    let booksService : IBooksService = BooksService(mockLogger, dbConnection)

    [<Test>]
    member this.``GetAllBooks should return books from database`` () =        
        let result = booksService.GetAllBooks().Result
    
        Assert.Equal(4, result |> Seq.length)
        
    [<Test>]
    member this.``GetGenreById should return books from database`` () =
        let result = booksService.GetBookById(1).Result
    
        Assert.Equal("ывмотвоматотволмтолтмвы", result.description)
        Assert.Equal("жить здорово", result.title)
        Assert.Equal("книга", result.type_book)
        Assert.Equal(1000, result.volume)
        Assert.Equal(2002, result.year)
         
    [<Test>]
    member this.``CreateGenre should create and return genre from database`` () =
        let book = {
            title = "test";
            type_book = "test";
            volume = 1234;
            year = 1999;
            description = "test2";
            genre_id = 1;
            author_id = 2;
        }
        let result = booksService.CreateBook(book).Result

        Assert.Equal("test2", result.description)
        Assert.Equal("test", result.title)
        Assert.Equal("test", result.type_book)
        Assert.Equal(1234, result.volume)
        Assert.Equal(1999, result.year)
             
