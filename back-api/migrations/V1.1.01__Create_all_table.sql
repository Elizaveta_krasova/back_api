drop table if exists UsersRoles cascade;
drop table if exists Users cascade;
drop table if exists СarsNumbers cascade;
drop table if exists Statuses cascade;
drop table if exists Statements cascade;

create table UsersRoles
(
    id int generated always as identity primary key,
    role_title text not null
);


create table Users
(
    id int generated always as identity primary key,
    login text not null,
    name text not null,
    middlename text not null,
    lastname text,
    email text not null,
    phonenumber varchar(50) not null,
    password text not null,
    role_id int not null references UsersRoles
);

create table authors
(
    id int generated always as identity primary key,
    name text not null
);

create table genres
(
    id int generated always as identity primary key,
    title text not null,
    description text
);

create table books
(
    id int generated always as identity primary key,
    title text not null,
    type_book text not null,
    volume int not null,
    year int,
    description text,
    genre_id int references genres,
    author_id int references authors
);
