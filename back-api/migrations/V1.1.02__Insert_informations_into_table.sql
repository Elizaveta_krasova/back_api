insert into UsersRoles (role_title)
values  ('simple_user'),
        ('admin');


insert into Users (login, name, middlename, lastname, email, phonenumber, password, role_id)
values  ('lizOk', 'Liza', 'Krasova', 'Aleksandrovna', 'krasova@mail.ru', '89132248984', '1234', 1),
        ('admin', 'admin', 'admin', 'admin', 'admin@test.ru', '8999999999', 'College2024', 2);


insert into authors (name)
values  ('Ноунейм'),
        ('Борода'),
        ('Ушинов'),
        ('Пушкин');


insert into genres (title, description)
values  ('лирика', 'jnfjsnvdjnfjvn'),
        ('поэма', 'jnfjsnvdvkmdjnfjvn'),
        ('сказка', 'jnfjsnvdvsdjnfjvn'),
        ('басня', 'sddcmvfm');


insert into books (title, type_book, volume, year, description, genre_id, author_id)
values  ('жить здорово', 'книга', 1000, 2002, 'ывмотвоматотволмтолтмвы', 1, 1),
        ('жить', 'книга', 200, 2022, 'ывмотвоматотволмтолтмвы', 1, 2),
        ('круто', 'книга', 1000, 1992, 'ывмотвоматотволмтолтмвы', 1, 3),
        ('о царе каком-то', 'повесть', 123, 2992, 'ывмотвоматотволмтолтмвы', 2, 4);




