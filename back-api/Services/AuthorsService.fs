namespace back_api.Services

open System.Data
open Microsoft.Extensions.Logging
open back_api.Api.Entites
open back_api.Api.Interfaces
open Dapper


type AuthorsService(logger: ILogger<AuthorsService>, pgConnection: IDbConnection) =
    
    let getAllAuthors (_) = task {
        try
            return! pgConnection.QueryAsync<GetAuthorResponse>("Select * from Authors;")
        with
        | err ->
            logger.LogError("getAllAuthors: {err}", err)
            return failwith err.Message
    }
    
    let getAuthorById (getAuthorByIdRequests : int) = task {
        try
            let query = "Select * from Authors where id = @id;"
            let parameters = dict [("id", box getAuthorByIdRequests)]
            let! res = pgConnection.QueryAsync<GetAuthorResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("getAuthorById: {err}", err)
            return failwith err.Message
    }
    
    let createAuthor (createAuthorRequests : CreateAuthorRequests) = task {
        try
            let query = "
Insert into Authors (name)
values (@name)
returning id, name;"
            let parameters = dict [("name", box createAuthorRequests.name)]
            let! res = pgConnection.QueryAsync<GetAuthorResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("createAuthor: {err}", err)
            return failwith err.Message
    }
    
        
    interface IAuthorsService with
        member _.GetAllAuthors() = getAllAuthors ()
        member _.GetAuthorById getBookeByIdRequests = getAuthorById getBookeByIdRequests
        member _.CreateAuthor createBookRequests = createAuthor createBookRequests

