namespace back_api.Services

open System.Data
open Microsoft.Extensions.Logging
open back_api.Api.Entites
open back_api.Api.Interfaces
open Dapper
open BCrypt.Net


type UsersService(logger: ILogger<UsersService>, pgConnection: IDbConnection) =
        
    let userLogin (userLoginRequest : UserLogin) = task {
        try
            let query = "
Select id, login, role_id, password from Users 
where login = @login and password = @password;"
            let parameters = dict [("login", box userLoginRequest.login); ("password", userLoginRequest.password);]
            
            let! res = pgConnection.QueryAsync<UserLoginResponse>(query, parameters)
            return Seq.head res
            // let result = Seq.head res
            // let isValid = BCrypt.Verify(userLoginRequest.password, result.password)
            // match isValid with
            // | true -> return {
            //     UserLoginResponse.id = result.id;
            //     UserLoginResponse.login = result.login
            //     UserLoginResponse.role_id = result.role_id }
            // | false -> return failwith "password not valid"
        with
        | err ->
            logger.LogError("userLogin: {err}", err)
            return failwith err.Message
    }
    
    let userExists (login : string) = task {
        try
            let query = "
Select id, login, role_id, password from Users 
where login = @login"
            let parameters = dict [("login", box login);]
            
            let! res = pgConnection.QueryAsync<UserLoginResponse>(query, parameters)
            match res with
            | null -> return false
            | _ -> return true
        with
        | err ->
            logger.LogError("userLogin: {err}", err)
            return failwith err.Message
    }
    
    let userRegistration (userRegistrationRequests : UserRegistration) = task {
        try
            // let! bool = userExists userRegistrationRequests.login
            // match bool with
            // | false -> return failwith "user exists"
            // | true ->

            match Helper.validateEmail userRegistrationRequests.email with
            | false -> return failwith "email not valid"
            | _ ->

            match Helper.validatePhoneNumber userRegistrationRequests.phonenumber with
            | false -> return failwith "phonenumber not valid"
            | _ -> 

            let query = "
Insert into Users (login, name, middlename, lastname, email, phonenumber, password, role_id)
values  (@login, @name, @middlename, @lastname, @email, @phonenumber, @password, @role_id)
returning id, login, name, middlename, lastname, email, phonenumber, role_id;"

            // let hashPassword = Helper.encryptPassword userRegistrationRequests.password |> string
            let parameters = dict [
                ("name", box userRegistrationRequests.name); ("middlename", userRegistrationRequests.middlename)
                ("lastname", userRegistrationRequests.lastname); ("email", userRegistrationRequests.email);
                ("login", userRegistrationRequests.login); ("password", userRegistrationRequests.password);
                ("phonenumber", userRegistrationRequests.phonenumber); ("role_id", 1);
            ]
            
            let! res = pgConnection.QueryAsync<User>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("createRole: {err}", err)
            return failwith err.Message
    }
    
        
    let getAllUsers (_) = task {
        try
            return! pgConnection.QueryAsync<User>("Select * from Users;")
        with
        | err ->
            logger.LogError("getAllUsers: {err}", err)
            return failwith err.Message
    }
    
    let getUserById (userId : int) = task {
        try
            let query = "Select * from Users where id = @id;"
            let parameters = dict [("id", box userId)]
            let! res = pgConnection.QueryAsync<User>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("getUserById: {err}", err)
            return failwith err.Message
    }
    
    let deleteUser (deleteGenreRequest: int) = task {
        try
            let query = "
delete from Users
where id = @id
returning id, login, name, middlename, lastname, email, phonenumber, role_id;"
            let parameters = dict [("id", box deleteGenreRequest)]
            let! res = pgConnection.QueryAsync<User>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("deleteUser: {err}", err)
            return failwith err.Message
    }


    
    interface IUsersService with
        member _.UserLogin userLoginRequest = userLogin userLoginRequest
        member _.UserRegistration userRegistrationRequest = userRegistration userRegistrationRequest
        member _.GetUserById getRoleByIdRequests = getUserById getRoleByIdRequests
        member _.DeleteUser deleteUserRequests = deleteUser deleteUserRequests
        member _.GetAllUsers() = getAllUsers()
