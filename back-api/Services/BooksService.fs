namespace back_api.Services

open System.Data
open Microsoft.Extensions.Logging
open back_api.Api.Entites
open back_api.Api.Interfaces
open Dapper


type BooksService(logger: ILogger<BooksService>, pgConnection: IDbConnection) =
    
    let getAllBooks (_) = task {
        try
            return! pgConnection.QueryAsync<GetBookResponse>("
Select b.id, b.title, b.type_book, b.volume, b.year, b.description, g.title as genre_title, a.name as author_name from Books b
left join authors a on a.id = b.author_id
left join genres g on g.id = b.genre_id;")
        with
        | err ->
            logger.LogError("getAllBooks: {err}", err)
            return failwith err.Message
    }
    
    let getBookById (getBookByIdRequests : int) = task {
        try
            let query = "
Select b.id, b.title, b.type_book, b.volume, b.year, b.description, g.title as genre_title, a.name as author_name from Books b
left join authors a on a.id = b.author_id
left join genres g on g.id = b.genre_id
where b.id = @id;"
            let parameters = dict [("id", box getBookByIdRequests)]
            let! res = pgConnection.QueryAsync<GetBookResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("getBookById: {err}", err)
            return failwith err.Message
    }
    
    let createBook (createBookRequest : CreateBookRequest) = task {
        try
            let query = "
insert into books (title, type_book, volume, year, description, genre_id, author_id)
values  (@title, @type_book, @volume, @year, @description, @genre_id, @author_id)
returning id;"
            let parameters = dict [("title", box createBookRequest.title); ("type_book", createBookRequest.type_book)
                                   ("volume", createBookRequest.volume); ("year", createBookRequest.year)
                                   ("description", createBookRequest.description); ("genre_id", createBookRequest.genre_id)
                                   ("author_id", createBookRequest.author_id);]
            let! res = pgConnection.QueryAsync<GetBookResponse>(query, parameters)
            
            let! book = getBookById (Seq.head res).id
            return book
        with
        | err ->
            logger.LogError("createBook: {err}", err)
            return failwith err.Message
    }
    
    
    interface IBooksService with
        member _.GetAllBooks() = getAllBooks ()
        member _.GetBookById getBookByIdRequests = getBookById getBookByIdRequests
        member _.CreateBook createBookRequests = createBook createBookRequests

