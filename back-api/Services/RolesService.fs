namespace back_api.Services

open System.Data
open Microsoft.Extensions.Logging
open back_api.Api.Entites
open back_api.Api.Interfaces
open Dapper


type RolesService(logger: ILogger<RolesService>, pgConnection: IDbConnection) =
    
    let getAllRoles (_) = task {
        try
            return! pgConnection.QueryAsync<GetRoleResponse>("Select * from UsersRoles;")
        with
        | err ->
            logger.LogError("getAllRoles: {err}", err)
            return failwith err.Message
    }
    
    let getRoleById (getRoleByIdRequests : int) = task {
        try
            let query = "Select * from UsersRoles where id = @id;"
            let parameters = dict [("id", box getRoleByIdRequests)]
            let! res = pgConnection.QueryAsync<GetRoleResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("getRoleById: {err}", err)
            return failwith err.Message
    }
    
    let getRoleByIds (getRoleByIdsRequests : GetRoleByIdsRequests) = task {
        try
            let query = "
Select *
from UsersRoles
where id = ANY(ARRAY[@id]);"
            let parameters = dict [("id", box getRoleByIdsRequests.id)]
            let! res = pgConnection.QueryAsync<GetRoleResponse>(query, parameters)
            return res
        with
        | err ->
            logger.LogError("getRoleByIds: {err}", err)
            return failwith err.Message
    }
    
    let createRole (createRoleRequests : CreateRoleRequests) = task {
        try
            let query = "
Insert into UsersRoles (role_title)
values (@role_title)
returning id, role_title;"
            let parameters = dict [("role_title", box createRoleRequests.role_title)]
            let! res = pgConnection.QueryAsync<GetRoleResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("createRole: {err}", err)
            return failwith err.Message
    }
    
    let updateRole (updateRoleRequests : UpdateRoleRequests) = task {
        try
            let query = "
update UsersRoles 
set role_title = @role_title
where id = @id 
returning id, role_title;"
            let parameters = dict [("role_title", box updateRoleRequests.role_title); ("id", updateRoleRequests.id)]
            let! res = pgConnection.QueryAsync<GetRoleResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("updateRole: {err}", err)
            return failwith err.Message
    }

    let deleteRole (deleteRoleRequest: DeleteRoleRequest) = task {
        try
            let query = "
delete from UsersRoles
where id = @id
returning id, role_title;"
            let parameters = dict [("id", box deleteRoleRequest.id)]
            let! res = pgConnection.QueryAsync<GetRoleResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("deleteRole: {err}", err)
            return failwith err.Message
    }
        
    
    interface IRolesService with
        member _.GetAllRoles() = getAllRoles ()
        member _.GetRoleById getRoleByIdRequests = getRoleById getRoleByIdRequests
        member _.GetRoleByIds getRoleByIdsRequests = getRoleByIds getRoleByIdsRequests
        member _.CreateRole createRoleRequests = createRole createRoleRequests
        member _.UpdateRole updateRoleRequests = updateRole updateRoleRequests
        member _.DeleteRole deleteRoleRequests = deleteRole deleteRoleRequests

