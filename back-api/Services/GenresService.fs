namespace back_api.Services

open System.Data
open Microsoft.Extensions.Logging
open back_api.Api.Entites
open back_api.Api.Interfaces
open Dapper


type GenresService(logger: ILogger<GenresService>, pgConnection: IDbConnection) =
    
    let getAllGenres (_) = task {
        try
            return! pgConnection.QueryAsync<GetGenreResponse>("Select * from Genres;")
        with
        | err ->
            logger.LogError("getAllGenres: {err}", err)
            return failwith err.Message
    }
    
    let getGenreById (getGenreByIdRequests : int) = task {
        try
            let query = "
Select * from Genres 
where id = @id;"
            let parameters = dict [("id", box getGenreByIdRequests)]
            let! res = pgConnection.QueryAsync<GetGenreResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("getGenreById: {err}", err)
            return failwith err.Message
    }
    
    let createGenre (createGenreRequest : CreateGenreRequest) = task {
        try
            let query = "
insert into Genres (title, description)
values  (@title, @description)
returning id, title, description;"
            let parameters = dict [("title", box createGenreRequest.title);("description", createGenreRequest.description)]
            let! res = pgConnection.QueryAsync<GetGenreResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("createGenre: {err}", err)
            return failwith err.Message
    }
    
    let deleteGenre (deleteGenreRequest: int) = task {
        try
            let query = "
delete from Genres
where id = @id
returning id, title, description;"
            let parameters = dict [("id", box deleteGenreRequest)]
            let! res = pgConnection.QueryAsync<GetGenreResponse>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("deleteGenre: {err}", err)
            return failwith err.Message
    }
        

    
    
    interface IGenresService with
        member _.GetAllGenres() = getAllGenres ()
        member _.GetGenreById getGenreByIdRequests = getGenreById getGenreByIdRequests
        member _.CreateGenre createGenreRequests = createGenre createGenreRequests
        member _.DeleteGenre deleteGenreRequests = deleteGenre deleteGenreRequests

