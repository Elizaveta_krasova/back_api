module back_api.Services.Helper

open System.Security.Cryptography
open System.Text
open System.Text.RegularExpressions
open System

let Encrypt(text:string, key:string, iv:string) =
    use aes = new AesCryptoServiceProvider()
    aes.Key <- Encoding.ASCII.GetBytes(key)
    aes.IV <- Encoding.ASCII.GetBytes(iv)
    
    let encryptor = aes.CreateEncryptor(aes.Key, aes.IV)
    use msEncrypt = new System.IO.MemoryStream()
    use csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)
    
    let toEncrypt = Encoding.ASCII.GetBytes(text)
    csEncrypt.Write(toEncrypt, 0, toEncrypt.Length)
    csEncrypt.FlushFinalBlock()
    
    Encoding.ASCII.GetString(msEncrypt.ToArray())

let Decrypt(encryptedText:string, key:string, iv:string) =
    use aes = new AesCryptoServiceProvider()
    aes.Key <- Encoding.ASCII.GetBytes(key)
    aes.IV <- Encoding.ASCII.GetBytes(iv)
    
    let decryptor = aes.CreateDecryptor(aes.Key, aes.IV)
    use msDecrypt = new System.IO.MemoryStream()
    use csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Write)
    
    let toDecrypt = Encoding.ASCII.GetBytes(encryptedText)
    csDecrypt.Write(toDecrypt, 0, toDecrypt.Length)
    csDecrypt.FlushFinalBlock()
    
    Encoding.ASCII.GetString(msDecrypt.ToArray())


let validateEmail (email:string) =
    let pattern = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
    let regex = new Regex(pattern)
    regex.IsMatch(email)

let validatePhoneNumber (phoneNumber:string) =
    let pattern = @"^\+7 \([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}$"   //  +7 (XXX) XXX-XX-XX
    let regex = new Regex(pattern)
    regex.IsMatch(phoneNumber)