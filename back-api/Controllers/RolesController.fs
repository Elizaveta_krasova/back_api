namespace FSharpBackApi.Controllers

open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open back_api.Api.Entites
open back_api.Api.Interfaces


/// <summary>
/// Контроллер для сущности Роль
/// </summary>
[<ApiController>]
[<Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)>]
[<Route("api/v1/roles")>]
type RolesController(logger: ILogger<RolesController>,
                     rolesService: IRolesService) =
    inherit ControllerBase()
    
    /// <summary>
    /// Получение всех ролей
    /// </summary>
    /// <returns>GetRoleResponse</returns>
    [<HttpGet>]
    member this.GetAllRoles() = task {
        try
            let! roles = rolesService.GetAllRoles()

            match Seq.isEmpty roles with
                | true -> return this.NotFound() |> ActionResult<seq<GetRoleResponse>>
                | false -> return this.Ok(roles) |> ActionResult<seq<GetRoleResponse>>
        with
        | err ->
            logger.LogError("GetAllRoles: {err}", err)
            return this.StatusCode(500) |> ActionResult<seq<GetRoleResponse>>
    }
    
    /// <summary>
    /// Получение роли по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор роли</param>
    /// <returns>GetRoleResponse</returns>
    [<HttpGet>]
    [<Route("{id}")>]
    member this.GetRoleById(id: int) = task {
        try
            let! role = rolesService.GetRoleById id

            match box role with
                | null -> return this.NotFound() |> ActionResult<GetRoleResponse>
                | _ -> return this.Ok(role) |> ActionResult<GetRoleResponse>
        with
        | err ->
            logger.LogError("GetRolesById: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetRoleResponse>
    }
    
    /// <summary>
    /// Получение ролей по идентификаторам
    /// </summary>
    /// <param name="getRoleByIdsRequests">Идентификаторы ролей</param>
    /// <returns>GetRoleResponse</returns>
    [<HttpPost>]
    [<Route("by_ids")>]
    member x.GetRoleByIds([<FromBody>] getRoleByIdsRequests: GetRoleByIdsRequests) = task {
        try
            let! role = rolesService.GetRoleByIds(getRoleByIdsRequests)
            
            match box role with
                | null -> return x.NotFound() |> ActionResult<seq<GetRoleResponse>>
                | _ -> return x.Ok(role) |> ActionResult<seq<GetRoleResponse>>
        with
        | err ->
            logger.LogError("GetRolesByIds: {err}", err)
            return x.StatusCode(500) |> ActionResult<seq<GetRoleResponse>>
    }
    
    /// <summary>
    /// Содание роли
    /// </summary>
    /// <param name="createRoleRequests">Название роли</param>
    /// <returns>GetRoleResponse</returns>
    [<Authorize(Policy = "AdminPolicy")>]
    [<HttpPost>]
    [<Route("create")>]
    member this.CreateRole([<FromBody>] createRoleRequests: CreateRoleRequests) = task {
        try
            let! role = rolesService.CreateRole(createRoleRequests)
            
            match box role with
            | null -> return this.BadRequest() |> ActionResult<GetRoleResponse>
            | _ -> return this.Ok(role) |> ActionResult<GetRoleResponse>
        with
        | err ->
            logger.LogError("CreateRole: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetRoleResponse>
    }
    
    /// <summary>
    /// Обновление роли по идентификатору
    /// </summary>
    /// <param name="updateRoleRequests">Идентификатор роли и название</param>
    /// <returns>GetRoleResponse</returns>
    [<Authorize(Policy = "AdminPolicy")>]
    [<HttpPut>]
    [<Route("update")>]
    member this.UpdateRole([<FromBody>] updateRoleRequests: UpdateRoleRequests) = task {
        try
            let! role = rolesService.GetRoleById(updateRoleRequests.id)
            match box role with
            | null -> return this.NotFound() |> ActionResult<GetRoleResponse>
            | _ ->

            let! updateRole = rolesService.UpdateRole(updateRoleRequests)
            match box updateRole with
            | null -> return this.BadRequest() |> ActionResult<GetRoleResponse>
            | _ -> return this.Ok(updateRole) |> ActionResult<GetRoleResponse>
        with
        | err ->
            logger.LogError("UpdateRole: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetRoleResponse>
    }
    
    /// <summary>
    /// Удаление роли по идентификатору
    /// </summary>
    /// <param name="deleteRoleRequests">Идентификатор роли</param>
    /// <returns>GetRoleResponse</returns>
    [<Authorize(Policy = "AdminPolicy")>]
    [<HttpDelete>]
    member this.DeleteRole([<FromBody>] deleteRoleRequests: DeleteRoleRequest) = task {
        try
            let! role = rolesService.GetRoleById(deleteRoleRequests.id)
            match box role with
            | null -> return this.NotFound() |> ActionResult<GetRoleResponse>
            | _ ->

            let! role = rolesService.DeleteRole(deleteRoleRequests)
            match box role with
            | null -> return this.BadRequest() |> ActionResult<GetRoleResponse>
            | _ -> return this.Ok(role) |> ActionResult<GetRoleResponse>
        with
        | err ->
            logger.LogError("DeleteRole: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetRoleResponse>
    }