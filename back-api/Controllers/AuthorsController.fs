namespace FSharpBackApi.Controllers

open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open back_api.Api.Entites
open back_api.Api.Interfaces


/// <summary>
/// Контроллер для сущности Автор
/// </summary>
[<ApiController>]
[<Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)>]
[<Route("api/v1/authors")>]
type AuthorsController(logger: ILogger<AuthorsController>,
                     rolesService: IAuthorsService) =
    inherit ControllerBase()
    
    /// <summary>
    /// Получение всех авторов
    /// </summary>
    /// <returns>GetAuthorResponse</returns>
    [<HttpGet>]
    member this.GetAllRoles() = task {
        try
            let! authors = rolesService.GetAllAuthors()

            match Seq.isEmpty authors with
                | true -> return this.NotFound() |> ActionResult<seq<GetAuthorResponse>>
                | false -> return this.Ok(authors) |> ActionResult<seq<GetAuthorResponse>>
        with
        | err ->
            logger.LogError("GetAllRoles: {err}", err)
            return this.StatusCode(500) |> ActionResult<seq<GetAuthorResponse>>
    }
    
    /// <summary>
    /// Получение автора по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор роли</param>
    /// <returns>GetAuthorResponse</returns>
    [<HttpGet>]
    [<Route("{id}")>]
    member this.GetAuthorById(id: int) = task {
        try
            let! author = rolesService.GetAuthorById id

            match box author with
                | null -> return this.NotFound() |> ActionResult<GetAuthorResponse>
                | _ -> return this.Ok(author) |> ActionResult<GetAuthorResponse>
        with
        | err ->
            logger.LogError("GetRolesById: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetAuthorResponse>
    }
    
    /// <summary>
    /// Содание автора
    /// </summary>
    /// <param name="createAuthorRequests">Название автора</param>
    /// <returns>GetAuthorResponse</returns>
    [<Authorize(Policy = "AdminPolicy")>]
    [<HttpPost>]
    [<Route("create")>]
    member this.CreateAuthor([<FromBody>] createAuthorRequests: CreateAuthorRequests) = task {
        try
            let! author = rolesService.CreateAuthor(createAuthorRequests)
            
            match box author with
            | null -> return this.BadRequest() |> ActionResult<GetAuthorResponse>
            | _ -> return this.Ok(author) |> ActionResult<GetAuthorResponse>
        with
        | err ->
            logger.LogError("CreateAuthor: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetAuthorResponse>
    }
    