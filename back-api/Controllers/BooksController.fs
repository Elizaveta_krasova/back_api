namespace FSharpBackApi.Controllers

open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open back_api.Api.Entites
open back_api.Api.Interfaces


/// <summary>
/// Контроллер для сущности книга
/// </summary>
[<ApiController>]
[<Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)>]
[<Route("api/v1/books")>]
type BooksController(logger: ILogger<BooksController>,
                     booksService: IBooksService) =
    inherit ControllerBase()
    
    /// <summary>
    /// Получение всех книг
    /// </summary>
    /// <returns>GetBookResponse</returns>
    [<HttpGet>]
    member this.GetAllBook() = task {
        try
            let! authors = booksService.GetAllBooks()

            match Seq.isEmpty authors with
                | true -> return this.NotFound() |> ActionResult<seq<GetBookResponse>>
                | false -> return this.Ok(authors) |> ActionResult<seq<GetBookResponse>>
        with
        | err ->
            logger.LogError("GetAllBook: {err}", err)
            return this.StatusCode(500) |> ActionResult<seq<GetBookResponse>>
    }
    
    /// <summary>
    /// Получение книги по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор книги</param>
    /// <returns>GetBookResponse</returns>
    [<HttpGet>]
    [<Route("{id}")>]
    member this.GetBookById(id: int) = task {
        try
            let! author = booksService.GetBookById id

            match box author with
                | null -> return this.NotFound() |> ActionResult<GetBookResponse>
                | _ -> return this.Ok(author) |> ActionResult<GetBookResponse>
        with
        | err ->
            logger.LogError("GetBookById: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetBookResponse>
    }
    
    /// <summary>
    /// Содание книги
    /// </summary>
    /// <param name="createBookRequest">Название книги</param>
    /// <returns>GetBookResponse</returns>
    [<Authorize(Policy = "AdminPolicy")>]
    [<HttpPost>]
    [<Route("create")>]
    member this.CreateBook([<FromBody>] createBookRequest: CreateBookRequest) = task {
        try
            let! book = booksService.CreateBook(createBookRequest)
            
            match box book with
            | null -> return this.BadRequest() |> ActionResult<GetBookResponse>
            | _ -> return this.Ok(book) |> ActionResult<GetBookResponse>
        with
        | err ->
            logger.LogError("CreateBook: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetBookResponse>
    }
    