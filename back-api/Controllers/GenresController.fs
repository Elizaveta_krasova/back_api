namespace FSharpBackApi.Controllers

open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open back_api.Api.Entites
open back_api.Api.Interfaces


/// <summary>
/// Контроллер для сущности Жанры
/// </summary>
[<ApiController>]
[<Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)>]
[<Route("api/v1/genres")>]
type GenresController(logger: ILogger<GenresController>,
                     genresService: IGenresService) =
    inherit ControllerBase()
    
    /// <summary>
    /// Получение всех Жанров
    /// </summary>
    /// <returns>GetGenreResponse</returns>
    [<HttpGet>]
    member this.GetAllGenre() = task {
        try
            let! genres = genresService.GetAllGenres()

            match Seq.isEmpty genres with
                | true -> return this.NotFound() |> ActionResult<seq<GetGenreResponse>>
                | false -> return this.Ok(genres) |> ActionResult<seq<GetGenreResponse>>
        with
        | err ->
            logger.LogError("GetAllGenre: {err}", err)
            return this.StatusCode(500) |> ActionResult<seq<GetGenreResponse>>
    }
    
    /// <summary>
    /// Получение Жанра по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор роли</param>
    /// <returns>GetGenreResponse</returns>
    [<HttpGet>]
    [<Route("{id}")>]
    member this.GetBookById(id: int) = task {
        try
            let! genre = genresService.GetGenreById id

            match box genre with
                | null -> return this.NotFound() |> ActionResult<GetGenreResponse>
                | _ -> return this.Ok(genre) |> ActionResult<GetGenreResponse>
        with
        | err ->
            logger.LogError("GetGenreById: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetGenreResponse>
    }
    
    /// <summary>
    /// Содание Жанра
    /// </summary>
    /// <param name="createGenreRequest">Название роли</param>
    /// <returns>GetGenreResponse</returns>
    [<Authorize(Policy = "AdminPolicy")>]
    [<HttpPost>]
    [<Route("create")>]
    member this.CreateGenre([<FromBody>] createGenreRequest: CreateGenreRequest) = task {
        try
            let! genre = genresService.CreateGenre(createGenreRequest)
            
            match box genre with
            | null -> return this.BadRequest() |> ActionResult<GetGenreResponse>
            | _ -> return this.Ok(genre) |> ActionResult<GetGenreResponse>
        with
        | err ->
            logger.LogError("CreateGenre: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetGenreResponse>
    }
    
    /// <summary>
    /// Удаление жанра по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор роли</param>
    /// <returns>GetRoleResponse</returns>
    [<Authorize(Policy = "AdminPolicy")>]
    [<HttpDelete>]
    [<Route("{id}")>]
    member this.DeleteRole(id: int) = task {
        try
            let! genre = genresService.GetGenreById id
            match box genre with
            | null -> return this.NotFound() |> ActionResult<GetRoleResponse>
            | _ ->

            let! genre = genresService.DeleteGenre id
            match box genre with
            | null -> return this.BadRequest() |> ActionResult<GetRoleResponse>
            | _ -> return this.Ok(genre) |> ActionResult<GetRoleResponse>
        with
        | err ->
            logger.LogError("DeleteGenre: {err}", err)
            return this.StatusCode(500) |> ActionResult<GetRoleResponse>
    }