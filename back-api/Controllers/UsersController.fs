namespace FSharpBackApi.Controllers

open System.IdentityModel.Tokens.Jwt
open System.Security.Claims
open System.Text
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open System
open Microsoft.IdentityModel.Tokens
open back_api.Api.Interfaces
open back_api.Api.Responses
open back_api.Api.Entites
open back_api.Authentication.Common


[<ApiController>]
[<Route("api/v1/users")>]
type UsersController ( logger : ILogger<UsersController>,
                       rolesService: IRolesService,
                       usersService: IUsersService) =
    inherit ControllerBase()
    
    [<HttpPost>]
    [<Route("login")>]
    member this.Login([<FromBody>] loginRequest: UserLogin) = task {
        try            
            let! user = usersService.UserLogin(loginRequest)
            
            match box user with
            | null -> return this.NotFound() |> ActionResult<AuthResponse>
            | _ ->
                
            let! userRole = rolesService.GetRoleById user.role_id
                
            let claims = seq { Claim(ClaimTypes.Role, userRole.role_title) };
            
            let jwt = JwtSecurityToken(
                AuthOptions.ISSUER,
                AuthOptions.AUDIENCE,
                claims,
                DateTime.Now.Add(TimeSpan.FromMinutes(-60)),
                DateTime.UtcNow.Add(TimeSpan.FromHours(10)),
                SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256))
            
            let encodedJwt = JwtSecurityTokenHandler().WriteToken(jwt);
            
            let response = { access_token = encodedJwt; user_id = user.id; login = user.login };
            
            return this.Ok(response) |> ActionResult<AuthResponse>
        with
        | err ->
             logger.LogError("Login: {err}", err)
             return this.Unauthorized() |> ActionResult<AuthResponse>
    }
    
    [<HttpPost>]
    [<Route("registration")>]
    member this.Registration([<FromBody>] userRequest: UserRegistration) = task {
        try
            let! user = usersService.UserRegistration(userRequest)
            
            match box user with
            | null -> return this.BadRequest() |> ActionResult<User>
            | _ -> return this.Ok(user) |> ActionResult<User>
        with
        | err ->
             logger.LogError("registration: {err}", err)
             return this.StatusCode(500) |> ActionResult<User>
    }
    
    /// <summary>
    /// Получение всех пользователей
    /// </summary>
    /// <returns>User</returns>
    [<HttpGet>]
    member this.GetAllUsers() = task {
        try
            let! users = usersService.GetAllUsers()

            match Seq.isEmpty users with
                | true -> return this.NotFound() |> ActionResult<seq<User>>
                | false -> return this.Ok(users) |> ActionResult<seq<User>>
        with
        | err ->
            logger.LogError("GetAllRoles: {err}", err)
            return this.StatusCode(500) |> ActionResult<seq<User>>
    }
    
    /// <summary>
    /// Получение юзера по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор роли</param>
    /// <returns>User</returns>
    [<HttpGet>]
    [<Route("{id}")>]
    member this.GetRoleById(id: int) = task {
        try
            let! user = usersService.GetUserById id

            match box user with
                | null -> return this.NotFound() |> ActionResult<User>
                | _ -> return this.Ok(user) |> ActionResult<User>
        with
        | err ->
            logger.LogError("GetRolesById: {err}", err)
            return this.StatusCode(500) |> ActionResult<User>
    }

    
    /// <summary>
    /// Удаление юзер пао идентификатору
    /// </summary>
    /// <param name="id">Идентификатор роли</param>
    /// <returns>User</returns>
    [<Authorize(Policy = "AdminPolicy")>]
    [<HttpDelete>]
    [<Route("{id}")>]
    member this.DeleteUser(id: int) = task {
        try
            let! user = usersService.GetUserById(id)
            match box user with
            | null -> return this.NotFound() |> ActionResult<User>
            | _ ->
    
            let! user = usersService.DeleteUser(id)
            match box user with
            | null -> return this.BadRequest() |> ActionResult<User>
            | _ -> return this.Ok(user) |> ActionResult<User>
        with
        | err ->
            logger.LogError("DeleteRole: {err}", err)
            return this.StatusCode(500) |> ActionResult<User>
    }
    
    [<HttpGet>]
    [<Route("~/admin")>]
    member x.GetAutocompletePage() = task {
        return x.Content(System.IO.File.ReadAllText "./wwwroot/autocomplete.html", "text/html", Encoding.UTF8)
    }

