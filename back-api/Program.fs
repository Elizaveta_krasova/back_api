namespace FSharpApiSample

open System.Collections.Generic
open System.Data
open System.Security.Claims
open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open Microsoft.IdentityModel.Protocols.OpenIdConnect
open Microsoft.IdentityModel.Tokens
open Microsoft.OpenApi.Models
open Npgsql
open Prometheus.DotNetRuntime
open StackExchange.Redis
open back_api.Api.Interfaces
open back_api.Authentication.Common
open back_api.Services
open Prometheus

module Program =
    DotNetEnv.Env.Load() |> ignore
    
    let private pgConnectionString = $"""
Server={DotNetEnv.Env.GetString("POSTGRES_HOST", "127.0.0.1")};
Port={DotNetEnv.Env.GetString "POSTGRES_PORT"};
Database={DotNetEnv.Env.GetString "POSTGRES_DB"};
User Id={DotNetEnv.Env.GetString "POSTGRES_USER"};
Password={DotNetEnv.Env.GetString "POSTGRES_PASSWORD"};
"""

    let private redisConnectionString = $"""
{DotNetEnv.Env.GetString("REDIS_HOST", "127.0.0.1")}:{DotNetEnv.Env.GetString "REDIS_PORT"},
password={DotNetEnv.Env.GetString "REDIS_PASSWORD"}
"""

    [<EntryPoint>]
    let main args =
        let builder = WebApplication.CreateBuilder args

        builder.Services.AddControllers() |> ignore
        builder.Services.AddEndpointsApiExplorer() |> ignore
        builder.Services.AddSwaggerGen(fun opt ->
            opt.AddSecurityDefinition("Bearer", OpenApiSecurityScheme(
                Name = "Authorization",
                Description = "Enter the Bearer Authorization string as following: `Bearer Generated-JWT-Token`",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer"))

            let req = OpenApiSecurityRequirement()
            req.Add(OpenApiSecurityScheme(
                Name = "Bearer", In = ParameterLocation.Header,
                Reference = OpenApiReference(Id = "Bearer", Type = ReferenceType.SecurityScheme)), List<string>())

            opt.AddSecurityRequirement(req)
        ) |> ignore
  
        builder.Services.AddSpaStaticFiles(fun conf -> conf.RootPath <- "wwwroot")

        let frontendDevPolicy = "frontend_dev_policy"
        builder.Services.AddCors(fun options ->
            options.AddPolicy(frontendDevPolicy, fun policyBuilder ->
                policyBuilder
                    .WithOrigins("*")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                |> ignore)) |> ignore

        builder.Services.AddScoped<IDbConnection>(fun _ -> new NpgsqlConnection(pgConnectionString)) |>ignore
        builder.Services.AddSingleton(ConnectionMultiplexer.Connect(redisConnectionString).GetDatabase()) |> ignore
                
        builder.Services.AddSingleton<IUsersService, UsersService>() |> ignore
        builder.Services.AddSingleton<IRolesService, RolesService>() |> ignore
        builder.Services.AddSingleton<IAuthorsService, AuthorsService>() |> ignore
        builder.Services.AddSingleton<IBooksService, BooksService>() |> ignore
        builder.Services.AddSingleton<IGenresService, GenresService>() |> ignore
        
        
        builder.Services.AddAuthentication(
            JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer( fun x ->
                (
                    x.RequireHttpsMetadata <- false
                    x.TokenValidationParameters <- new TokenValidationParameters(
                        ValidateAudience = false,
                        ValidateIssuer = false,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey()
                    )
                    x.Configuration <- new OpenIdConnectConfiguration()
                )
        ) |> ignore
        builder.Services.AddAuthorization(
            fun opt -> opt.AddPolicy("AdminPolicy", (new AuthorizationPolicyBuilder())
                                                        .RequireAuthenticatedUser()
                                                        .RequireClaim(ClaimTypes.Role, "admin").Build())
            ) |> ignore 
        
        builder.Services.UseHttpClientMetrics() |> ignore
        
        DotNetRuntimeStatsBuilder.Default().StartCollecting() |> ignore
        
        let app = builder.Build()        
        
        if app.Environment.IsDevelopment() then
            app.UseCors(frontendDevPolicy) |> ignore

        app.UseStaticFiles() |> ignore
        app.UseHttpsRedirection() |> ignore
        app.UseRouting() |> ignore
        app.UseHttpMetrics() |> ignore
        
        app.UseAuthentication() |> ignore
        app.UseAuthorization() |> ignore
        
        app.UseEndpoints(fun builder ->
            builder.MapControllers() |> ignore
            builder.MapMetrics() |> ignore
        ) |> ignore
        app.UseSwagger() |> ignore
        app.UseSwaggerUI() |> ignore
        app.Map("", fun (applicationBuilder: IApplicationBuilder) ->
            applicationBuilder.UseSpa(fun spaBuilder -> spaBuilder.Options.SourcePath <- "/wwwroot")) |> ignore

        app.Run()

        0
