namespace back_api.Api.Interfaces

open System.Collections.Generic
open System.Threading.Tasks
open back_api.Api.Entites

type IGenresService =
    abstract GetAllGenres: unit -> Task<IEnumerable<GetGenreResponse>>
    abstract GetGenreById: getGenreByIdRequest: int -> Task<GetGenreResponse>
    abstract CreateGenre: createGenreRequest: CreateGenreRequest -> Task<GetGenreResponse>
    abstract DeleteGenre: deleteGenreRequests: int -> Task<GetGenreResponse>
    // abstract UpdateAuthor: updateAuthorRequests: UpdateRoleRequests -> Task<GetAuthorResponse>
