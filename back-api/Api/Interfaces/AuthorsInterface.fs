namespace back_api.Api.Interfaces

open System.Collections.Generic
open System.Threading.Tasks
open back_api.Api.Entites

type IAuthorsService =
    abstract GetAllAuthors: unit -> Task<IEnumerable<GetAuthorResponse>>
    abstract GetAuthorById: getAuthorByIdRequests: int -> Task<GetAuthorResponse>
    abstract CreateAuthor: createAuthorRequests: CreateAuthorRequests -> Task<GetAuthorResponse>
    // abstract UpdateAuthor: updateAuthorRequests: UpdateRoleRequests -> Task<GetAuthorResponse>
    // abstract DeleteAuthor: deleteAuthorRequests: DeleteRoleRequest -> Task<GetAuthorResponse>
