namespace back_api.Api.Interfaces

open System.Collections.Generic
open System.Threading.Tasks
open back_api.Api.Entites

type IUsersService =
    abstract UserLogin: userLogin: UserLogin -> Task<UserLoginResponse>
    abstract UserRegistration: userRegistration: UserRegistration -> Task<User>
    abstract GetAllUsers: unit -> Task<IEnumerable<User>>
    abstract GetUserById: getRoleByIdRequests: int -> Task<User>
    abstract DeleteUser: deleteUserRequests: int -> Task<User>


