namespace back_api.Api.Interfaces

open System.Collections.Generic
open System.Threading.Tasks
open back_api.Api.Entites

type IRolesService =
    abstract GetAllRoles: unit -> Task<IEnumerable<GetRoleResponse>>
    abstract GetRoleById: getRoleByIdRequests: int -> Task<GetRoleResponse>
    abstract GetRoleByIds: getRoleByIdsRequests: GetRoleByIdsRequests -> Task<IEnumerable<GetRoleResponse>>
    abstract CreateRole: createRoleRequests: CreateRoleRequests -> Task<GetRoleResponse>
    abstract UpdateRole: updateRoleRequests: UpdateRoleRequests -> Task<GetRoleResponse>
    abstract DeleteRole: deleteRoleRequests: DeleteRoleRequest -> Task<GetRoleResponse>
