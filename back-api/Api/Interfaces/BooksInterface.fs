namespace back_api.Api.Interfaces

open System.Collections.Generic
open System.Threading.Tasks
open back_api.Api.Entites

type IBooksService =
    abstract GetAllBooks: unit -> Task<IEnumerable<GetBookResponse>>
    abstract GetBookById: getBookByIdRequests: int -> Task<GetBookResponse>
    abstract CreateBook: createBookRequests: CreateBookRequest -> Task<GetBookResponse>
    // abstract UpdateAuthor: updateAuthorRequests: UpdateRoleRequests -> Task<GetAuthorResponse>
    // abstract DeleteAuthor: deleteAuthorRequests: DeleteRoleRequest -> Task<GetAuthorResponse>
