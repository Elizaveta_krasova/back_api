namespace back_api.Api.Requests

[<CLIMutable>]
type LoginRequest = {
    login: string
    password: string
}

[<CLIMutable>]
type RegRequest = {
    login: string
    name: string
    middlename: string
    lastname: string
    email: string
    phonenumber: string
    password: string
    role_id: int
}