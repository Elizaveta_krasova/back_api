namespace back_api.Api.Responses

[<CLIMutable>]
type AuthResponse = {
    access_token: string
    user_id: int
    login: string
}

[<CLIMutable>]
type RegResponse = {
    email: string
}

