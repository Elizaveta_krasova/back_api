module back_api.Api.Entites

open System

[<CLIMutable>]
type User = {
    id: int
    login: string
    name: string
    middlename: string
    lastname: string
    email: string
    phonenumber: string
    role_id: int
}

[<CLIMutable>]
type UserWithPassword = {
    id: int
    login: string
    role_id: int
    password: string
}

[<CLIMutable>]
type UserLoginResponse = {
    id: int
    login: string
    role_id: int
}

[<CLIMutable>]
type UserRegistration = {
    login: string
    name: string
    middlename: string
    lastname: string
    email: string
    phonenumber: string
    password: string
    role_id: int
}

[<CLIMutable>]
type UserLogin = {
    login: string
    password: string
}

[<CLIMutable>]
type Role = {
    id: int
    role_title: string
}

[<CLIMutable>]
type GetRoleResponse = {
    id: int
    role_title: string
}

[<CLIMutable>]
type GetAuthorResponse = {
    id: int
    name: string
}

[<CLIMutable>]
type GetBookResponse = {
    id: int
    title: string
    type_book: string
    volume: int
    year: int
    description: string
    genre_title: string
    author_name: string
}

[<CLIMutable>]
type GetGenreResponse = {
    id: int
    title: string
    description: string
}

[<CLIMutable>]
type CreateGenreRequest = {
    title: string
    description: string
}

[<CLIMutable>]
type CreateBookRequest = {
    title: string
    type_book: string
    volume: int
    year: int
    description: string
    genre_id: int
    author_id: int
}


[<CLIMutable>]
type GetStatementResponse = {
    id: int
    description: string
    registration_number: string
    status_name: string
    name: string
}

[<CLIMutable>]
type GetStatement2Response = {
    id: int
    description: string
    car_id: int
    status_id: int
    user_id: int
}

[<CLIMutable>]
type UpdateStatementRequests = {
    id: int
    description: string
    car_id: int
    status_id: int
    user_id: int
}

[<CLIMutable>]
type CreateStatementRequests = {
    description: string
    car_id: int
    status_id: int
    user_id: int
}

[<CLIMutable>]
type GetStatementDeleteResponse = {
    id: int
}

[<CLIMutable>]
type GetRoleByIdRequests = {
    id: int
}

[<CLIMutable>]
type GetStatementByIdRequests = {
    id: int
}

[<CLIMutable>]
type GetRoleByIdsRequests = {
    id: int[]
}

[<CLIMutable>]
type CreateRoleRequests = {
    role_title: string
}

[<CLIMutable>]
type CreateAuthorRequests = {
    name: string
}

[<CLIMutable>]
type UpdateRoleRequests = {
    id: int
    role_title: string
}

[<CLIMutable>]
type DeleteRoleRequest = {
    id: int
}

[<CLIMutable>]
type DeleteStatementRequest = {
    id: int
}


[<CLIMutable>]
type NewUser = { Name: string; Email: string }

type Email = Email of string

module Email =
    let value (Email email) = email

    let create (emailStr: string) =
        let errors =
            [| if String.IsNullOrWhiteSpace emailStr then
                   "Empty email"

               if not <| emailStr.Contains "@" then
                   "Invalid email" |]

        if errors |> Array.isEmpty then
            emailStr.ToLower().Trim() |> Email |> Ok
        else
            errors |> Error

module NewUser =
    let parseUser (info: NewUser) =
        let problems =
            [ match Email.create info.Email with
              | Error errors -> nameof info.Email, errors
              | Ok e -> ()

              nameof info.Name,
              [| if String.IsNullOrWhiteSpace info.Name then
                     "Empty name"
                 if info.Name.Length < 3 then
                     "Short name" |] ]

        problems
        |> List.filter (fun (_, errs) -> errs.Length > 0)
        |> function
            | [] ->
                { 
                  Name = info.Name
                  Email = info.Email }
                |> Ok
            | errors -> errors |> dict |> Error