module back_api.Authentication.Common

open System.Text
open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.IdentityModel.Tokens

let [<Literal>] CookieJwtSchemes =
    JwtBearerDefaults.AuthenticationScheme
    
module AuthOptions =
    DotNetEnv.Env.Load() |> ignore
    let [<Literal>] ISSUER = "http://10.0.75.1:8080";
    let [<Literal>] AUDIENCE = "http://localhost:8080";
    let key = DotNetEnv.Env.GetString "SECRET_KEY"
    let GetSymmetricSecurityKey() = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
