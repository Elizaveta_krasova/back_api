# back_api

Для начала работы нужно склонировать репозиторий.
```
git clone git@gitlab.com:Elizaveta_krasova/back_api.git
```

Далее создать .env файл в директории проекта back-api. Пример содержания файла:

```
POSTGRES_HOST=localhost
POSTGRES_PORT=5432
POSTGRES_USER=postgres
POSTGRES_DB=postgres
POSTGRES_PASSWORD=postgres

POSTGRES_HOST_TEST=localhost
POSTGRES_PORT_TEST=5431
POSTGRES_USER_TEST=postgres
POSTGRES_DB_TEST=postgres
POSTGRES_PASSWORD_TEST=postgres

APP_NAME=backApi
APP_PORT=8080

REDIS_PORT=6789
REDIS_PASSWORD=qwerty

PROMETHEUS_PORT=8000
GRAFANA_PORT=3030
SECRET_KEY=my-32-character-ultra-secure-and-ultra-long-secret
```
Чтобы поднять проект в docker нужно в директории back-api выполнить команду:

```
sudo docker compose up --build
```

Чтобы отчистить контейнер:

```
sudo docker compose down --remove-orphans
```

Для просмотра прометеуса зайти по ссылке:

```
http://localhost:PROMETHEUS_PORT/
```

Для просмотра графаны зайти по ссылке:

```
http://localhost:GRAFANA_PORT/
```

# Какие технологии использованы?

.Net, EntityFramework, Postgres, Flyway, Docker, Prometheus, Grafana и ещё миллион всего

# А что в проекте есть сейчас?

- Проект упакован в docker-compose
- Миграции при разворачивании проекта
- Авторизация пользователей
- Аутентификация пользователей
- Запросы на просмотр и создание ролей
- Запросы на просмотр курсов
- Логирование запросов
- Валидация входящих данных
- Метрики (localhost:APP_PORT/metrics)



![img_2.png](img_2.png)