module ApiTests

open System.Net.Http
open NUnit.Framework

// [<SetUp>]
// let Setup () =
//     ()
//
// [<Test>]
// let Test1 () =
//     Assert.Pass()
    
// Функция для выполнения HTTP-запроса к API
let makeHttpRequest (url: string) =
    let clientHandler = new HttpClientHandler();

    // Pass the handler to httpclient(from you are calling api)
    let client = new HttpClient(clientHandler);
    let response = client.GetAsync(url).Result
    response.Content.ReadAsStringAsync().Result

[<TestFixture>]
type ApiTests() =

    // Тест проверки успешного выполнения API метода
    [<Test>]
    member this.``Should_Return_Successful_Response``() =
        // Подставьте URL вашего API метода
        let url = "https://localhost:8080/api/v1/roles"
        let response = makeHttpRequest(url)
        Assert.That(response, Contains.Substring("Success"))

    // Тест проверки корректности возвращаемых данных API метода
    // [<Test>]
    // member this.``Should_Return_Correct_Data``() =
    //     // Подставьте URL вашего API метода
    //     let url = "https://api.example.com/method"
    //     let response = makeHttpRequest(url)
    //     Assert.That(response, Contains.Substring("ExpectedData"))
